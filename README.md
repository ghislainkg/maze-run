# Project : Maze run

This project is a game similar to the famous game Pacman. I implement it only for training and demonstration purpose.

### Description
This pacman-like is made by intensively using **OpenGL** and **GLM**. The other third party libraries used will be listed bellow.

In the game, the player control the character whose goal is to collect all bonus points on the map while avoiding being eaten by the many moving enemies in the map.

We have many entities present in the game:
* The **map** which is a 2D discret grid. At each position of the grid, we have empty space, a wall or another entity.
* The **player** who can move in four directions.
* The **enemies** who can also move in four directions, can eat the player when they touch him and appears to move randomly.
* The **bonus points** that a represented by fruits on the map. By touching them, the player increases its score and by touching all of them, he wins.

![alt text](imgs/game.png "Game")
![alt text](imgs/menu.png "Menu")

# Build and Run

To build the project:
* create a *./build* directory in the project main directory.
* run the following commands from the project main directory
```
cmake . -B build
make -C build
```

To run the game after the build:
* From the main directory, run the executable file
```
./mazerun
```

# Third party libraries

* [Glad](https://github.com/Dav1dde/glad) for OpenGL calls. (MIT Licence)
* [GLM](https://glm.g-truc.net/) for maths with OpenGL. (MIT Licence)
* [GLFW](https://www.glfw.org/) for OpenGL context and window system. (zlib/libpng License, a BSD-like license)
* [STB](https://github.com/nothings/stb) for image loading and writing. (MIT Licence)
* [FreeType](https://freetype.org/) for text character glyphs textures. (FreeType Licence, a BSD-like license)

# License

The license follows the 4-clause BSD license.
Here are the key points:

Redistribution and use in source and binary forms, with or without modification, are
permitted provided that the following conditions are met:
* Redistributions of source code must retain the above copyright notice,
  this list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation 
  and/or other materials provided with the distribution.

* All advertising materials mentioning features or use of this software must display 
  the following acknowledgement:
  This product includes software developed by the Ghislain Kengne Gumete.

* Neither the name of Ghislain Kengne Gumete nor the names of its contributors may
  be used to endorse or promote products derived from this software without specific
  prior written permission.
