/*
GameLoop.hpp
-----------------------------------------------------

Copyright (c) 2023, Ghislain Kengne Gumete
All rights reserved.

License follow the 4-clause BSD license.

Created on: 29 august 2023
    Author: Ghislain Kengne Gumete
      Mail: kengneghislain98@gmail.com

*/

#ifndef _MAZE_RUN__HANDLERS_GAME_LOOP_
#define _MAZE_RUN__HANDLERS_GAME_LOOP_

#include "../map/Map.hpp"

enum GameState {
  WIN, LOOSE, CONTINU
};

class GameLoop {
public:
  GameLoop() {}

  void setMap(MapPtr m);

  void onPlayerMove(const glm::ivec2 & vec);
  void loop();

  inline EntityPtr getPlayer() {return player;}

  inline GameState getGameState() const {return gameState;}

private:
  EntityPtr player;
  MapPtr map;

  CountDownClock playerMoveClock = CountDownClock(300);

  GameState gameState = CONTINU;

  void updateState();
};

#endif

