/*
GameEditor.hpp
-----------------------------------------------------

Copyright (c) 2023, Ghislain Kengne Gumete
All rights reserved.

License follow the 4-clause BSD license.

Created on: 01 septemer 2023
    Author: Ghislain Kengne Gumete
      Mail: kengneghislain98@gmail.com

*/

#include "GameEditor.hpp"

void GameEditor::onClickAddEntity(const glm::vec2 & gridPos) {
  if(map==nullptr) return;
  
  if(!countDown.getIsRunning()) {
    countDown.start();
  }
  if(countDown.isExpired()) {
    auto ent = map->get(gridPos);
    if(ent) {
      if(ent->getType() == WALL) {
        BonusPtr bonus = std::make_shared<Bonus>("Bonus");
        map->set(gridPos, bonus);
      }
      else if(ent->getType() == BONUS) {
        EnemyPtr enemy = std::make_shared<Enemy>("Enemy");
        map->set(gridPos, enemy);
      }
      else if(ent->getType() == ENEMY) {
        PlayerPtr player = std::make_shared<Player>("Player");
        map->set(gridPos, player);
      }
      else if(ent->getType() == PLAYER) {
        WallPtr wall = std::make_shared<Wall>("", HORIZONTAL);
        map->set(gridPos, wall);
      }
    }
    else {
      WallPtr wall = std::make_shared<Wall>("", HORIZONTAL);
      map->set(gridPos, wall);
    }

    countDown.start();
  }
  else {
    countDown.run();
  }

}

void GameEditor::onClickEraseEntity(const glm::vec2 & gridPos) {
  if(map==nullptr) return;

  if(!countDown.getIsRunning()) {
    countDown.start();
  }

  if(countDown.isExpired()) {
    auto ent = map->get(gridPos);
    if(ent) {
      map->set(gridPos, nullptr);
    }

    countDown.start();
  }
  else {
    countDown.run();
  }
}
