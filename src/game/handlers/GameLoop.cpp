/*
GameLoop.cpp
-----------------------------------------------------

Copyright (c) 2023, Ghislain Kengne Gumete
All rights reserved.

License follow the 4-clause BSD license.

Created on: 29 august 2023
    Author: Ghislain Kengne Gumete
      Mail: kengneghislain98@gmail.com

*/

#include "GameLoop.hpp"

void GameLoop::setMap(MapPtr m) {
  gameState = GameState::CONTINU;
  map = m;
  if(map==nullptr) return;
  uint W = map->getWidth();
  uint H = map->getHeight();
  auto players = map->findAllEntities(EntityType::PLAYER);
  if(players.size()>=1) {
    player = players.front();
  }
}

void GameLoop::onPlayerMove(const glm::ivec2 & vec) {
  if(map==nullptr) return;
  if(player==nullptr) return;

  if(!playerMoveClock.getIsRunning()) {
    std::cout << "Start" << std::endl;
    playerMoveClock.start();
  }

  playerMoveClock.print();

  if(playerMoveClock.isExpired()) {
    auto onCollision = [](EntityPtr playerEnt, EntityPtr other) {
      Player * player = static_cast<Player*>(playerEnt.get());
      if(other->getType()==EntityType::WALL) {
        return -1;
      }
      else if(other->getType()==EntityType::ENEMY) {
        player->decrementLife();
        if(player->getLifeCount()==0) {
          return -1;
        }
        else {
          return 0;
        }
      }
      else if(other->getType()==EntityType::BONUS) {
        player->incrementPoints();
        return 0;
      }
      else {
        return 0;
      }
    };
    map->moveEntity(vec, player, onCollision);
    updateState();

    playerMoveClock.start();
  }
  else {
    playerMoveClock.run();
  }
}

void GameLoop::loop() {
  if(map==nullptr) return;

  auto enemies = map->findAllEntities(EntityType::ENEMY);
  for(auto enemy : enemies) {
    int r = getRandInt(0, 4);
    glm::ivec2 vec;
    if(r==0) vec = {0,1};
    if(r==1) vec = {0,-1};
    if(r==2) vec = {1,0};
    if(r==3) vec = {-1,0};
    auto onCollision = [](EntityPtr enemyEnt, EntityPtr other) {
      Enemy * enemy = static_cast<Enemy*>(enemyEnt.get());
      if(other->getType()==EntityType::WALL) {
        return -1;
      }
      else if(other->getType()==EntityType::ENEMY) {
        return -1;
      }
      else if(other->getType()==EntityType::BONUS) {
        return -1;
      }
      else if(other->getType()==EntityType::PLAYER) {
        Player * player = static_cast<Player*>(other.get());
        player->decrementLife();
        if(player->getLifeCount()==0) {
          return 0;
        }
        else {
          return 1;
        }
      }
      else {
        return 0;
      }
    };
    map->moveEntity(vec, enemy, onCollision);
  }
  
  updateState();
}

void GameLoop::updateState() {
  Player * player = static_cast<Player*>(this->player.get());
  if(player->getLifeCount() == 0) {
    this->gameState = GameState::LOOSE;
  }

  if(map->findAllEntities(EntityType::BONUS).size()==0) {
    this->gameState = GameState::WIN;
  }
}
