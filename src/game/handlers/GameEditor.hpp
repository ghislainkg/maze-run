/*
GameEditor.hpp
-----------------------------------------------------

Copyright (c) 2023, Ghislain Kengne Gumete
All rights reserved.

License follow the 4-clause BSD license.

Created on: 01 septemer 2023
    Author: Ghislain Kengne Gumete
      Mail: kengneghislain98@gmail.com

*/

#ifndef _MAZE_RUN__HANDLERS_GAME_EDITOR_
#define _MAZE_RUN__HANDLERS_GAME_EDITOR_

#include "../map/Map.hpp"

class GameEditor {
public:
  GameEditor() {}

  void onClickAddEntity(const glm::vec2 & gridPos);
  void onClickEraseEntity(const glm::vec2 & gridPos);

  inline MapPtr getMap() {return map;}
  inline void setMap(MapPtr m) {map = m;}

private:
  MapPtr map;

  CountDownClock countDown = CountDownClock(300);
};

#endif
