/*
Player.hpp
-----------------------------------------------------

Copyright (c) 2023, Ghislain Kengne Gumete
All rights reserved.

License follow the 4-clause BSD license.

Created on: 29 august 2023
    Author: Ghislain Kengne Gumete
      Mail: kengneghislain98@gmail.com

*/

#ifndef _MAZE_RUN__ENTITIES_PLAYER_
#define _MAZE_RUN__ENTITIES_PLAYER_

#include "../Entity.hpp"

class Player : public Entity {
public:
  Player(const std::string & name)
  : Entity(name) {}

  inline uint getLifeCount() const {return lifeCount;}
  inline uint getPoints() const {return points;}

  inline void incrementLife(uint f=1) { lifeCount+=f; }
  inline void decrementLife(uint f=1) { lifeCount-=f; }
  inline void incrementPoints(uint f=1) { points+=f; }

  inline void setLife(uint f) {lifeCount=f;}
  inline void setPoints(uint f) {points=f;}

  inline EntityType getType() override {return PLAYER;}

private:
  uint lifeCount = 0;
  uint points = 0;
};

typedef std::shared_ptr<Player> PlayerPtr;

#endif
