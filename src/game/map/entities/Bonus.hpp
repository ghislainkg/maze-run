/*
Bonus.hpp
-----------------------------------------------------

Copyright (c) 2023, Ghislain Kengne Gumete
All rights reserved.

License follow the 4-clause BSD license.

Created on: 29 august 2023
    Author: Ghislain Kengne Gumete
      Mail: kengneghislain98@gmail.com

*/

#ifndef _MAZE_RUN__ENTITIES_BONUS_
#define _MAZE_RUN__ENTITIES_BONUS_

#include "../Entity.hpp"

class Bonus : public Entity {
public:
  Bonus(const std::string & name)
  : Entity(name) {}

  inline EntityType getType() override {return BONUS;}

private:

};

typedef std::shared_ptr<Bonus> BonusPtr;

#endif
