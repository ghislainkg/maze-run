/*
Enemy.hpp
-----------------------------------------------------

Copyright (c) 2023, Ghislain Kengne Gumete
All rights reserved.

License follow the 4-clause BSD license.

Created on: 29 august 2023
    Author: Ghislain Kengne Gumete
      Mail: kengneghislain98@gmail.com

*/

#ifndef _MAZE_RUN__ENTITIES_ENEMY_
#define _MAZE_RUN__ENTITIES_ENEMY_

#include "../Entity.hpp"

class Enemy : public Entity {
public:
  Enemy(const std::string & name)
  : Entity(name) {}

  inline EntityType getType() override {return ENEMY;}

private:

};

typedef std::shared_ptr<Enemy> EnemyPtr;

#endif
