/*
Map.hpp
-----------------------------------------------------

Copyright (c) 2023, Ghislain Kengne Gumete
All rights reserved.

License follow the 4-clause BSD license.

Created on: 29 august 2023
    Author: Ghislain Kengne Gumete
      Mail: kengneghislain98@gmail.com

*/

#ifndef _MAZE_RUN__ENTITIES_WALL_
#define _MAZE_RUN__ENTITIES_WALL_

#include "../Entity.hpp"

enum WallType {
  NONE, HORIZONTAL, VERTICAL, CROSS
};
inline WallType charToWallType(char c) {
  switch (c) {
  case 'H':
    return HORIZONTAL;
  case 'V':
    return VERTICAL;
  case 'C':
    return CROSS;
  }
  return NONE;
}
inline char wallTypeToChar(WallType t) {
  switch(t) {
    case HORIZONTAL:
      return 'H';
    case VERTICAL:
      return 'V';
    case CROSS:
      return 'C';
  }
  return 0;
}

class Wall : public Entity {
public:
  Wall(const std::string & name, WallType wallType)
  : Entity(name), wallType(wallType) {}

  inline EntityType getType() override {return WALL;}

  inline WallType getWallType() {return wallType;}

private:
  WallType wallType;
};

typedef std::shared_ptr<Wall> WallPtr;

#endif
