/*
Entity.hpp
-----------------------------------------------------

Copyright (c) 2023, Ghislain Kengne Gumete
All rights reserved.

License follow the 4-clause BSD license.

Created on: 29 august 2023
    Author: Ghislain Kengne Gumete
      Mail: kengneghislain98@gmail.com

*/

#ifndef _MAZE_RUN__MAP_ENTITY_
#define _MAZE_RUN__MAP_ENTITY_

#include "../Utils.hpp"

enum EntityType {
  PLAYER, WALL, ENEMY, BONUS
};

class Entity {
  friend class Map;
public:
  Entity(const std::string & name)
  : name(name) {}

  inline const glm::ivec2 & getPosition() const {return position;}
  inline const std::string & getName() const {return name;}

  virtual EntityType getType() = 0;

private:
  glm::ivec2 position;  
  std::string name;
};

typedef std::shared_ptr<Entity> EntityPtr;

#endif
