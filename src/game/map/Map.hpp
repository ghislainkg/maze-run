/*
Map.hpp
-----------------------------------------------------

Copyright (c) 2023, Ghislain Kengne Gumete
All rights reserved.

License follow the 4-clause BSD license.

Created on: 29 august 2023
    Author: Ghislain Kengne Gumete
      Mail: kengneghislain98@gmail.com

*/

#ifndef _MAZE_RUN__MAP_MAP_
#define _MAZE_RUN__MAP_MAP_

#include "Entity.hpp"
#include "entities/Player.hpp"
#include "entities/Wall.hpp"
#include "entities/Bonus.hpp"
#include "entities/Enemy.hpp"

typedef int (*CollisionCallback)(EntityPtr entity, EntityPtr dest);

typedef std::shared_ptr<class Map> MapPtr;

class Map {
public:
  Map(uint width, uint height)
  : width(width), height(height) {
    initGrid();
  }

  EntityPtr get(const glm::ivec2 & pos);
  bool set(const glm::ivec2 & pos, EntityPtr entity);

  void moveEntity(const glm::ivec2 & vec, EntityPtr entity, CollisionCallback collisionCallback);

  static MapPtr load(const std::string & filepath, std::string & message);
  bool save(const std::string & filepath, std::string & message);

  inline uint getWidth() const {return width;}
  inline uint getHeight() const {return height;}

  std::list<EntityPtr> findAllEntities(EntityType type);

  void print();

private:
  uint width, height;
  std::vector<std::vector<EntityPtr>> grid;

  void initGrid();
  void teleport(const glm::uvec2 & start, const glm::uvec2 & end);
};

#endif
