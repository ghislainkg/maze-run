/*
Map.cpp
-----------------------------------------------------

Copyright (c) 2023, Ghislain Kengne Gumete
All rights reserved.

License follow the 4-clause BSD license.

Created on: 29 august 2023
    Author: Ghislain Kengne Gumete
      Mail: kengneghislain98@gmail.com

*/

#include "Map.hpp"

void Map::initGrid() {
  std::vector<EntityPtr> col;
  col.resize(height, nullptr);
  grid.resize(width, col);
}

EntityPtr Map::get(const glm::ivec2 & pos) {
  if(
    pos.x >= width || pos.x<0 ||
    pos.y >= height || pos.y<0
  ) {
    return nullptr;
  }
  return grid[pos.x][pos.y];
}

bool Map::set(const glm::ivec2 & pos, EntityPtr entity) {
  if(
    pos.x < width && pos.x>=0 &&
    pos.y < height && pos.y>=0
  ) {
    grid[pos.x][pos.y] = entity;
    if(entity) entity->position = pos;
    return true;
  }
  return false;
}

/*
Copy the content from start to end and erase start.
What was previously at end is lost.
*/
void Map::teleport(const glm::uvec2 & start, const glm::uvec2 & end) {
  if(start==end) return;
  EntityPtr src = get(start);
  if(set(end, src)) {
    set(start, nullptr);
  }
}

void Map::moveEntity(
  const glm::ivec2 & vec, EntityPtr entity, CollisionCallback collisionCallback
) {
  EntityPtr dest = get(entity->position + vec);
  if(dest==nullptr) {
    teleport(entity->position, entity->position + vec);
  }
  else {
    int res = collisionCallback(entity, dest);
    if(res==0) {
      teleport(entity->position, entity->position + vec);
    }
    else if(res==1) {
      set(entity->position, nullptr);
    }
  }
}

std::list<EntityPtr> Map::findAllEntities(EntityType type) {
  std::list<EntityPtr> res;
  for(auto & col : grid) {
    for(EntityPtr entity : col) {
      if(entity && entity->getType()==type) {
        res.push_back(entity);
      }
    }
  }
  return res;
}

MapPtr Map::load(const std::string & filepath, std::string & message) {
  std::cout << "A" << std::endl;
  std::ifstream in(filepath);
  if(!in) {
    message = "Cannot open map file " + filepath;
    std::cerr << "Fail opening map file " << filepath << std::endl;
    return nullptr;
  }

  uint width, height;
  in >> width >> height;
  MapPtr map = std::make_shared<Map>(width, height);

  char type = 'A';
  while(type!='@') {
    in >> type;
    if(type == 'P') {
      // Player
      // P name life points posx posy
      std::string name;
      uint life, points;
      glm::ivec2 pos;
      in >> name >> life >> points >> pos.x >> pos.y;
      PlayerPtr player = std::make_shared<Player>(name);
      player->incrementLife(life);
      player->incrementPoints(points);
      map->set(pos, player);
    }
    else if(type == 'W') {
      // Wall
      // W walltype posx posy
      glm::ivec2 pos;
      char w;
      in >> w >> pos.x >> pos.y;
      WallType wallType = charToWallType(w);
      if(wallType!=NONE) {
        WallPtr wall = std::make_shared<Wall>("", wallType);
        map->set(pos, wall);
      }
    }
    else if(type == 'E') {
      // Enemy
      std::string name;
      glm::ivec2 pos;
      in >> name >> pos.x >> pos.y;
      EnemyPtr enemy = std::make_shared<Enemy>(name);
      map->set(pos, enemy);
    }
    else if(type == 'B') {
      // Bonus
      std::string name;
      glm::ivec2 pos;
      in >> name >> pos.x >> pos.y;
      BonusPtr bonus = std::make_shared<Bonus>(name);
      map->set(pos, bonus);
    }
  }

  return map;
}

bool Map::save(const std::string & filepath, std::string & message) {
  std::ofstream out(filepath);
  if(!out) {
    message = "Cannot open map file " + filepath;
    std::cerr << "Fail writing map file " << filepath << std::endl;
    return false;
  }

  out << width << " " << height << std::endl;

  for(auto & col : grid) {
    for(EntityPtr entity : col) {
      if(entity) {
        if(entity->getType() == EntityType::PLAYER) {
          Player* player = static_cast<Player*>(entity.get());
          out << 
            "P" <<
            player->getName() << " " <<
            player->getLifeCount() << " " <<
            player->getPoints() << " " <<
            player->getPosition().x << " " <<
            player->getPosition().y << std::endl;
        }
        else if(entity->getType() == EntityType::ENEMY) {
          Enemy* enemy = static_cast<Enemy*>(entity.get());
          out << 
            "E" <<
            enemy->getName() << " " <<
            enemy->getPosition().x << " " <<
            enemy->getPosition().y << std::endl;
        }
        else if(entity->getType() == EntityType::WALL) {
          Wall* wall = static_cast<Wall*>(entity.get());
          out << 
            "W" <<
            wallTypeToChar(wall->getWallType()) << " " <<
            wall->getPosition().x << " " <<
            wall->getPosition().y << std::endl;
        }
        else if(entity->getType() == EntityType::BONUS) {
          Bonus* bonus = static_cast<Bonus*>(entity.get());
          out << 
            "B" <<
            bonus->getName() << " " <<
            bonus->getPosition().x << " " <<
            bonus->getPosition().y << std::endl;
        }
      }
    }
  }

  out << '@';

  return true;
}

void Map::print() {
  std::cout << "Map :" << std::endl;
  for(auto & col : grid) {
    for(EntityPtr entity : col) {
      if(entity) {
        if(entity->getType() == EntityType::PLAYER) {
          std::cout << "P ";
        }
        else if(entity->getType() == EntityType::ENEMY) {
          std::cout << "E ";
        }
        else if(entity->getType() == EntityType::WALL) {
          std::cout << "W ";
        }
        else if(entity->getType() == EntityType::BONUS) {
          std::cout << "B ";
        }
      }
      else {
        std::cout << "- ";
      }
    }
    std::cout << std::endl;
  }
}
