/*
Utils.hpp
-----------------------------------------------------

Copyright (c) 2023, Ghislain Kengne Gumete
All rights reserved.

License follow the 4-clause BSD license.

Created on: 29 august 2023
    Author: Ghislain Kengne Gumete
      Mail: kengneghislain98@gmail.com

*/

#ifndef _MAZE_RUN__GAME_UTILS_
#define _MAZE_RUN__GAME_UTILS_

// Glad (for OpenGL functions)
#include <glad/glad.h>

// GLWF (for OpenGL context and window)
#include <GLFW/glfw3.h>

// GLM (for maths)
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <glm/gtx/vector_angle.hpp>
#include <glm/gtx/euler_angles.hpp>

// C++ STD library
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <list>
#include <set>
#include <string>
#include <cmath>
#include <memory>
#include <algorithm>
#include <functional>
#include <chrono>

inline std::ostream & operator<<(std::ostream & out, const glm::vec3 & v) {
  out << "(" << v.x << "  " << v.y << "  " << v.z << ")";
  return out;
}
inline std::ostream & operator<<(std::ostream & out, const glm::uvec3 & v) {
  out << "(" << v.x << "  " << v.y << "  " << v.z << ")";
  return out;
}
inline std::ostream & operator<<(std::ostream & out, const glm::vec2 & v) {
  out << "(" << v.x << "  " << v.y << ")";
  return out;
}

inline std::string file2String(const std::string &filename) {
  std::ifstream t(filename.c_str());
  std::stringstream buffer;
  buffer << t.rdbuf();
  return buffer.str();
}

#define getOpenGLError(where) {int e = glGetError();if(e != GL_NO_ERROR) {std::cout << "GL Error detected : " << where << " : " << e << std::endl;assert(false);}}
#define getOpenGLErrorNoAssert(where) {int e = glGetError();if(e != GL_NO_ERROR) {std::cout << "GL Error detected : " << where << " : " << e << std::endl;}}

typedef std::ratio<1l, 1000000000000l> pico;
typedef std::chrono::duration<long long, pico> picoseconds;
typedef std::ratio<1l, 1000000000l> nano;
typedef std::chrono::duration<long long, nano> nanoseconds;
typedef std::ratio<1l, 1000000l> micro;
typedef std::chrono::duration<long long, micro> microseconds;
typedef std::ratio<1l, 1000l> milli;
typedef std::chrono::duration<long long, milli> milliseconds;

inline float getTimeSeconds() {
  auto now = std::chrono::high_resolution_clock::now();
  auto timemillis = std::chrono::duration_cast<milliseconds>(now.time_since_epoch()).count();
  return timemillis / 1000.0f;
}

inline long long getTimeMillis() {
  auto now = std::chrono::high_resolution_clock::now();
  auto timemillis = std::chrono::duration_cast<milliseconds>(now.time_since_epoch()).count();
  return timemillis;
}

/*Count down in milliseconds.*/
class CountDownClock {
public:
  CountDownClock(int total) {
    this->total = total;
  };

  inline void start() {
    countDown = total;
    isRunning = true;
  };

  inline void stop() {
    isRunning = false;
  }

  inline void run() {
    if(isRunning) {
      auto t = getTimeMillis();
      countDown -= t-last;
      last = t;
    }
  };

  inline bool isExpired() {
    return countDown <= 0;
  };
  inline bool getIsRunning() {
    return isRunning;
  }

  inline void print() {
    std::cout << "CountDownClock("
      << "isRunning: " << (isRunning?"true":"false")
      << ", total:" << total
      << ", last:" << last
      << ", countDown:" << countDown
      << ")" << std::endl;
  }

private:
  long long countDown = 0;
  int total = 1000;
  long long last = 0;
  bool isRunning = false;
};

#define COLOR_RED glm::vec3(0.71f, 0.082f, 0.035f)
#define COLOR_GREEN glm::vec3(0.165f, 0.71f, 0.035f)
#define COLOR_BLUE glm::vec3(0.106f, 0.322f, 0.859f)
#define COLOR_YELLOW glm::vec3(0.859f, 0.835f, 0.106f)
#define COLOR_GRAY glm::vec3(0.388f, 0.388f, 0.388f)
#define COLOR_BLACK glm::vec3(0.0f, 0.0f, 0.0f)
#define COLOR_WHITE glm::vec3(1.0f, 1.0f, 1.0f)
#define COLOR_ORANGE glm::vec3(1.0f, 0.549f, 0.0f)
#define COLOR_PURPLE glm::vec3(0.463f, 0.0f, 1.0f)
#define COLOR_PINK glm::vec3(1.0f, 0.0f, 1.0f)

inline int getRandInt(int lowest, int highest) {
  return lowest + std::rand() % (highest-lowest+1);
}

#endif
