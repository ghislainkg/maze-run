/*
GameRenderer.hpp
-----------------------------------------------------

Copyright (c) 2023, Ghislain Kengne Gumete
All rights reserved.

License follow the 4-clause BSD license.

Created on: 29 august 2023
    Author: Ghislain Kengne Gumete
      Mail: kengneghislain98@gmail.com

*/

#ifndef _MAZE_RUN__RENDERING_GAME_RENDERER_
#define _MAZE_RUN__RENDERING_GAME_RENDERER_

#include "../map/Map.hpp"
#include "Sprite.hpp"
#include "TextLine.hpp"

class GameRenderer {
public:
  GameRenderer() {}

  void initSprites();

  void renderMap(
    MapPtr map,
    float viewPortWidth, float viewPortHeight
  );

  glm::vec2 fromMapGridToScreenPos(
    MapPtr map, const glm::ivec2 & pos
  ) const;
  glm::vec2 getMapGridBlockScreenScale(
    MapPtr map
  ) const;
  glm::uvec2 fromMouseScreenPosToGridPos(
    MapPtr map,
    const glm::vec2 & pos,
    float viewPortWidth, float viewPortHeight
  ) const;

  std::vector<TextLinePtr> textLines;

private:
  Sprite spritePlayer;
  Sprite spriteWall;
  Sprite spriteEnemy;
  Sprite spriteBonus;
};

#endif
