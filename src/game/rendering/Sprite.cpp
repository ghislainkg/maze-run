/*
Sprite.cpp
-----------------------------------------------------

Copyright (c) 2023, Ghislain Kengne Gumete
All rights reserved.

License follow the 4-clause BSD license.

Created on: 29 august 2023
    Author: Ghislain Kengne Gumete
      Mail: kengneghislain98@gmail.com

*/

#include "Sprite.hpp"

#define STB_IMAGE_IMPLEMENTATION
#include <stbi/stb_image.h>

std::vector<glm::vec2> Sprite::quadData = {
  {-1.f, 1.f},
  {-1.f, -1.f},
  {1.f, 1.f},
  {1.f, -1.f}
};
GLuint Sprite::quadDataBufferID = 0;
GLuint Sprite::quadDataVAO = 0;
bool Sprite::isQuadDataReady = false;
Program Sprite::program = Program();

void Sprite::initQuadDataAndProgram() {
  glGenVertexArrays(1, &quadDataVAO);
  glBindVertexArray(quadDataVAO);

  glGenBuffers(1, &quadDataBufferID);
  glBindBuffer(GL_ARRAY_BUFFER, quadDataBufferID);
  glBufferData(
    GL_ARRAY_BUFFER,
    4*2*sizeof(float),
    quadData.data(),
    GL_STATIC_DRAW
  );
  glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, 0);
  glEnableVertexAttribArray(0);

  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(0);

  getOpenGLError("Sprite Quad Data init");

  program.initProgram(
    "src/game/rendering/shaders/sprite/vertex.glsl",
    "src/game/rendering/shaders/sprite/fragment.glsl",
    nullptr
  );

  isQuadDataReady = true;
}
void Sprite::destroyAll() {
  program.destroy();
  glDeleteBuffers(1, &quadDataBufferID);
  glDeleteVertexArrays(1, &quadDataVAO);
  isQuadDataReady = false;
}

void Sprite::loadTexture(const std::string & filepath) {
  stbi_set_flip_vertically_on_load(true);
  textureData = stbi_load(
    filepath.c_str(),
    &textureWidth, &textureHeight,
    nullptr, 4);
  
  glGenTextures(1, &textureID);
  glBindTexture(GL_TEXTURE_2D, textureID);

  // Texture wrapping
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
  // Texture Interpolation
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  // Use mipmap
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
  getOpenGLError("Sprite texture params");

  // Send texture data to GPU
  // RGBA internal and external format
  glTexImage2D(
    GL_TEXTURE_2D, 0, GL_RGBA, textureWidth, textureHeight, 0, 
    GL_RGBA, GL_UNSIGNED_BYTE, textureData);
  getOpenGLError("Sprite Tex Image");

  // Generate mipmaps
  glGenerateMipmap(GL_TEXTURE_2D);

  // Free texture data
  stbi_image_free(textureData);
}

void Sprite::render(
  const glm::vec2 & position, const glm::vec2 & scale, float angle
) {
  if(!isQuadDataReady) initQuadDataAndProgram();

  GLuint prog = program.getProgramID();
  if(customProgram) {
    prog = customProgram->getProgramID();
  }
  glUseProgram(prog);
  getOpenGLError("Sprite Use Program");

  glUniform2f(
    glGetUniformLocation(prog, "position"),
    position.x, position.y
  );
  glUniform2f(
    glGetUniformLocation(prog, "scale"),
    scale.x, scale.y
  );
  glUniform1f(
    glGetUniformLocation(prog, "angle"),
    angle
  );
  glUniform3f(
    glGetUniformLocation(prog, "oneColor"),
    color.r, color.g, color.b
  );
  getOpenGLError("Sprite Uniforms");

  if(glIsTexture(textureID)) {
    glActiveTexture(GL_TEXTURE0+SpriteTextureUnits::TEXTURE_DATA);
    glBindTexture(GL_TEXTURE_2D, textureID);
    glUniform1i(
      glGetUniformLocation(prog, "textureData"),
      SpriteTextureUnits::TEXTURE_DATA);
    getOpenGLError("Sprite Texture Uniform");

    glBindVertexArray(quadDataVAO);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    glBindVertexArray(0);
  }

  getOpenGLError("Sprite Draw Arrays");
}

void Sprite::destroy() {
  if(glIsTexture(textureID)) glDeleteTextures(1, &textureID);
}
