/*
Program.hpp
-----------------------------------------------------

Copyright (c) 2023, Ghislain Kengne Gumete
All rights reserved.

License follow the 4-clause BSD license.

Created on: 29 august 2023
    Author: Ghislain Kengne Gumete
      Mail: kengneghislain98@gmail.com

*/

#ifndef _MAZE_RUN__RENDERING_PROGRAM_
#define _MAZE_RUN__RENDERING_PROGRAM_

#include "../Utils.hpp"

class Program {
public:
  Program() {}

  GLuint getProgramID() {return programID;}

  inline void initProgram(
    const char * vertexShaderFilename,
    const char * fragmentShaderFilename,
    const char * geometryShaderFilename
  ) {
    programID = glCreateProgram();
    if(vertexShaderFilename)
        loadShader(programID, GL_VERTEX_SHADER, vertexShaderFilename);
    if(fragmentShaderFilename)
        loadShader(programID, GL_FRAGMENT_SHADER, fragmentShaderFilename);
    if(geometryShaderFilename)
        loadShader(programID, GL_GEOMETRY_SHADER, geometryShaderFilename);
    glLinkProgram(programID);
    getOpenGLError("Program init");
  }

  inline void destroy() {
    glDeleteProgram(programID);
  }

protected:
  GLuint programID = 0;

  inline void loadShader(GLuint program, GLenum type, const std::string &shaderFilename) {
    GLuint shader = glCreateShader(type);
    std::string shaderSourceString = file2String(shaderFilename);
    const GLchar *shaderSource = (const GLchar *)shaderSourceString.c_str();
    glShaderSource(shader, 1, &shaderSource, NULL);
    glCompileShader(shader);
    GLint success;
    GLchar infoLog[512];
    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
    if(!success) {
      glGetShaderInfoLog(shader, 512, NULL, infoLog);
      std::cout << "ERROR in compiling " << shaderFilename << "\n\t" << infoLog << std::endl;
      assert(false);
    }
    glAttachShader(program, shader);
    glDeleteShader(shader);
  }

};

typedef std::shared_ptr<Program> ProgramPtr;

#endif
