/*
TextCharacters.hpp
-----------------------------------------------------

Copyright (c) 2023, Ghislain Kengne Gumete
All rights reserved.

License follow the 4-clause BSD license.

Created on: 02 september 2023
    Author: Ghislain Kengne Gumete
      Mail: kengneghislain98@gmail.com

*/

#ifndef _MAZE_RUN__RENDERING_TEXT_CHARACTERS_
#define _MAZE_RUN__RENDERING_TEXT_CHARACTERS_

#include "../Utils.hpp"

class TextCharacters {
public:
  TextCharacters() {}

  bool init(
    const std::string & glyphsFontFilepath,
    uint widthPixels,
    uint heightPixels,
    uint count=128
  );

  inline GLuint getCharacterTexture(char c) const {
    return textures[(int)c];
  }

  inline const glm::vec2 & getDim(char c) const {
    return dims[(int)c];
  }
  inline const glm::vec2 getMaxDim() const {
    if(dims.size()==0) return {0,0};
    glm::vec2 m = dims[0];
    for(auto & d : dims) {
      if(d.x > m.x) m.x = d.x;
      if(d.y > m.y) m.y = d.y;
    }
    return m;
  }

  void destroy();

  void print() const;

private:
  std::vector<GLuint> textures;
  std::vector<glm::vec2> dims;
  uint widthPixels, heightPixels;
};

#endif
