/*
TextCharacters.hpp
-----------------------------------------------------

Copyright (c) 2023, Ghislain Kengne Gumete
All rights reserved.

License follow the 4-clause BSD license.

Created on: 02 september 2023
    Author: Ghislain Kengne Gumete
      Mail: kengneghislain98@gmail.com

*/

#include "TextCharacters.hpp"

#include <ft2build.h>
#include FT_FREETYPE_H

bool TextCharacters::init(
  const std::string & glyphsFontFilepath,
  uint widthPixels,
  uint heightPixels,
  uint count
) {
  this->widthPixels = widthPixels;
  this->heightPixels = heightPixels;

  // Init FreetType
  FT_Library ft;
  if(FT_Init_FreeType(&ft)) {
    std::cerr << "Cannot init FreeType" << std::endl;
    return false;
  }

  // Load font
  FT_Face face; // Hold the glyphs
  if(FT_New_Face(ft, glyphsFontFilepath.c_str(), 0, &face)) {
    std::cerr << "Cannot init FreeType" << std::endl;
    return false;
  }

  FT_Set_Pixel_Sizes(face, widthPixels, heightPixels);
  for(char c=0; c<count; c++) {
    if(FT_Load_Char(face, c, FT_LOAD_RENDER)) {
      std::cerr << "Fail loading character '" << c << "'" << std::endl;
      return false;
    }

    GLuint texture;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glPixelStorei( GL_UNPACK_ALIGNMENT, 1);
    glTexImage2D(
      GL_TEXTURE_2D, 0, GL_RED, 
      face->glyph->bitmap.width, face->glyph->bitmap.rows,
      0, GL_RED, GL_UNSIGNED_BYTE,
      face->glyph->bitmap.buffer
    );

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    textures.push_back(texture);
    dims.push_back({
      face->glyph->bitmap.width,
      face->glyph->bitmap.rows
    });

    std::cout << "Character : " << c
      << " width=" << face->glyph->bitmap.width
      << " height=" << face->glyph->bitmap.rows
      << " texture=" << texture
      << " is texture : " << (glIsTexture(texture)?"Yes":"No")
      << std::endl;
  }

  return true;
}

void TextCharacters::destroy() {
  for(auto t : textures) {
    if(glIsTexture(t)) glDeleteTextures(1, &t);
  }
}

void TextCharacters::print() const {
  std::cout << "Character Textures" << std::endl;
  for(auto t : textures) {
    std::cout << t << " " << (glIsTexture(t)?"Yes":"No") << std::endl;
  }
}
