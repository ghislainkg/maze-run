/*
Sprite.hpp
-----------------------------------------------------

Copyright (c) 2023, Ghislain Kengne Gumete
All rights reserved.

License follow the 4-clause BSD license.

Created on: 29 august 2023
    Author: Ghislain Kengne Gumete
      Mail: kengneghislain98@gmail.com

*/

#ifndef _MAZE_RUN__RENDERING_SPRITE_
#define _MAZE_RUN__RENDERING_SPRITE_

#include "../Utils.hpp"
#include "Program.hpp"

enum SpriteTextureUnits {
  TEXTURE_DATA = 0
};

class Sprite {
public:
  Sprite() {}

  void loadTexture(const std::string & filepath);
  inline void setTexture(GLuint texture) {textureID=texture;}
  inline void setColor(const glm::vec3 & c) {color=c;}
  inline void setCustomProgram(ProgramPtr & p) {customProgram=p;}

  inline GLuint getTextureID() const {return textureID;}

  void render(
    const glm::vec2 & position, const glm::vec2 & scale=glm::vec2(1,1), float angle=0
  );

  void destroy();
  static void destroyAll();

private:
  static void initQuadDataAndProgram();

  static std::vector<glm::vec2> quadData;
  static GLuint quadDataBufferID;
  static bool isQuadDataReady;
  static GLuint quadDataVAO;

  static Program program;
  
  int textureWidth, textureHeight;
  unsigned char * textureData = nullptr;
  GLuint textureID = 0;
  glm::vec3 color = {0,0,0};

  ProgramPtr customProgram = nullptr;
};

typedef std::shared_ptr<Sprite> SpritePtr;

#endif
