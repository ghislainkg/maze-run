/*
TextLine.hpp
-----------------------------------------------------

Copyright (c) 2023, Ghislain Kengne Gumete
All rights reserved.

License follow the 4-clause BSD license.

Created on: 02 september 2023
    Author: Ghislain Kengne Gumete
      Mail: kengneghislain98@gmail.com

*/

#ifndef _MAZE_RUN__RENDERING_TEXT_LINE_
#define _MAZE_RUN__RENDERING_TEXT_LINE_

#include "TextCharacters.hpp"
#include "Sprite.hpp"

enum TextAlign {
  ALIGN_LEFT, ALIGN_CENTER, ALIGN_RIGHT
};

class TextLine {
public:
  TextLine(
    const TextCharacters & characters
  ): characters(characters) {}

  void updateText(const std::string & t);
  void render(float viewPortWidth, float viewPortHeight);
  void destroy();
  static void destroyAll();

  /*Position in screen space [-1,1]*/
  inline void setPosition(const glm::vec2 & p) {pos=p;}
  inline void setTextAlign(TextAlign a) {align=a;}
  inline void setColor(const glm::vec3 & c) {color=c;}
  inline void setCharDims(const glm::vec2 & v) {charDims=v;}

  inline const std::string & getText() const {return text;}

private:
  std::string text;
  std::list<Sprite*> sprites;

  static ProgramPtr textSpriteProgram;
  static void initTextSpriteProgram();
  static bool isTextSpriteProgramInit;

  glm::vec2 pos;
  TextAlign align = TextAlign::ALIGN_CENTER;
  glm::vec3 color = {0.0, 0.5, 0.0};

  const TextCharacters & characters;

  glm::vec2 charDims = {0.1f, 0.1f};
};

typedef std::shared_ptr<TextLine> TextLinePtr;

#endif
