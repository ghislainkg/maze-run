#version 420 core

layout(location=0) in vec2 vPosition;

uniform vec2 position;
uniform vec2 scale;
uniform float angle;

out FRAG {
  vec2 texCoord;
} frag;

vec2 rotate(vec2 v, float a) {
  float s = sin(a);
	float c = cos(a);
	mat2 m = mat2(c, s, -s, c);
	return m * v;
}

void main() {
  frag.texCoord = rotate(vPosition, angle);
  frag.texCoord = 0.5*frag.texCoord+vec2(0.5,0.5);

  vec2 pos = vPosition;
  pos = pos*scale;
  pos = pos + position;

  gl_Position = vec4(pos.x, pos.y, 0.0, 1.0);
}
