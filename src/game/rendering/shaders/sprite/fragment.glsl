#version 420 core

out vec4 color;

layout(binding=0) uniform sampler2D textureData;

uniform vec3 oneColor;

in FRAG {
  vec2 texCoord;
} frag;

void main() {
  color = texture(textureData, frag.texCoord).rgba;
  color += vec4(oneColor, 0.0);
}
