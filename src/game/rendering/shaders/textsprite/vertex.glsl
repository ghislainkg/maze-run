#version 420 core

layout(location=0) in vec2 vPosition;

uniform vec2 position;
uniform vec2 scale;

out FRAG {
  vec2 texCoord;
} frag;

void main() {
  frag.texCoord = vPosition*vec2(1,-1);
  frag.texCoord = 0.5*frag.texCoord+vec2(0.5,0.5);

  vec2 pos = vPosition;
  pos = pos*scale;
  pos = pos + position;

  gl_Position = vec4(pos.x, pos.y, 0.0, 1.0);
}
