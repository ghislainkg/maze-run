#version 420 core

out vec4 color;

layout(binding=0) uniform sampler2D textureData;

in FRAG {
  vec2 texCoord;
} frag;

uniform vec3 oneColor;

void main() {
  color = vec4(
    oneColor.r, oneColor.g, oneColor.b,
    texture(textureData, frag.texCoord).r
  );
}
