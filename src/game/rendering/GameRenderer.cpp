/*
GameRenderer.cpp
-----------------------------------------------------

Copyright (c) 2023, Ghislain Kengne Gumete
All rights reserved.

License follow the 4-clause BSD license.

Created on: 29 august 2023
    Author: Ghislain Kengne Gumete
      Mail: kengneghislain98@gmail.com

*/

#include "GameRenderer.hpp"

void GameRenderer::initSprites() {
  spritePlayer.loadTexture("assets/pig.png");
  spriteEnemy.loadTexture("assets/beetle.png");
  spriteBonus.loadTexture("assets/lemon.png");
  spriteWall.loadTexture("assets/wall.png");
}

glm::vec2 GameRenderer::fromMapGridToScreenPos(
  MapPtr map, const glm::ivec2 & pos
) const {
  uint W = map->getWidth();
  uint H = map->getHeight();
  auto scale = getMapGridBlockScreenScale(map);
  return {
    (pos.x-W/2.f) / (W/2) + scale.x,
    (-pos.y+H/2.f) / (H/2) - scale.y
  };
}
glm::vec2 GameRenderer::getMapGridBlockScreenScale(
  MapPtr map
) const {
  uint W = map->getWidth();
  uint H = map->getHeight();
  return {1.f/W,1.f/H};
}
glm::uvec2 GameRenderer::fromMouseScreenPosToGridPos(
  MapPtr map,
  const glm::vec2 & pos,
  float viewPortWidth, float viewPortHeight
) const {
  uint W = map->getWidth();
  uint H = map->getHeight();
  return {
    (pos.x/viewPortWidth)*W,
    (pos.y/viewPortHeight)*H,
  };
}

void GameRenderer::renderMap(
  MapPtr map,
  float viewPortWidth, float viewPortHeight
) {
  glClearColor(0.2,0.2,0.2,1);
  glClear(GL_COLOR_BUFFER_BIT);

  // Blending
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glBlendEquation(GL_FUNC_ADD);
  // Stencil
  glDisable(GL_STENCIL_TEST);
  // Face culling
  glDisable(GL_CULL_FACE);
  // Depth testing
  glDisable(GL_DEPTH_TEST);

  if(map) {
    uint W = map->getWidth();
    uint H = map->getHeight();
    for(uint x=0; x<W; x++) {
      for(uint y=0; y<H; y++) {
        auto en = map->get({x,y});
        if(en) {
          glm::vec2 screenPos = fromMapGridToScreenPos(map, {x,y});
          glm::vec2 screenScale = getMapGridBlockScreenScale(map);
          if(en->getType() == EntityType::BONUS) {
            spriteBonus.render(screenPos, screenScale*0.8f, 0.f);
          }
          else if(en->getType() == EntityType::PLAYER) {
            spritePlayer.render(screenPos, screenScale*0.8f, 0.f);
          }
          else if(en->getType() == EntityType::ENEMY) {
            spriteEnemy.render(screenPos, screenScale*0.8f, 0.f);
          }
          else if(en->getType() == EntityType::WALL) {
            spriteWall.render(screenPos, screenScale*1.0f, 0.f);
          }
        }
      }
    }
  }

  for(auto textLine : textLines) {
    textLine->render(viewPortWidth, viewPortHeight);
  }
}
