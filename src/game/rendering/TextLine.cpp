/*
TextLine.hpp
-----------------------------------------------------

Copyright (c) 2023, Ghislain Kengne Gumete
All rights reserved.

License follow the 4-clause BSD license.

Created on: 02 september 2023
    Author: Ghislain Kengne Gumete
      Mail: kengneghislain98@gmail.com

*/

#include "TextLine.hpp"

ProgramPtr TextLine::textSpriteProgram = nullptr;
bool TextLine::isTextSpriteProgramInit = false;

void TextLine::initTextSpriteProgram() {
  textSpriteProgram = std::make_shared<Program>();
  textSpriteProgram->initProgram(
    "src/game/rendering/shaders/textsprite/vertex.glsl",
    "src/game/rendering/shaders/textsprite/fragment.glsl",
    nullptr
  );
  isTextSpriteProgramInit = true;
}

void TextLine::updateText(const std::string & t) {
  if(!isTextSpriteProgramInit) {
    initTextSpriteProgram();
  }

  text = t;
  for(auto s : sprites) {
    s->setTexture(0);
    delete s;
  }
  sprites.clear();
  for(char c : text) {
    Sprite * sprite = new Sprite();
    sprite->setTexture(characters.getCharacterTexture(c));
    sprite->setCustomProgram(textSpriteProgram);
    sprites.push_back(sprite);
  }
}

void TextLine::render(float viewPortWidth, float viewPortHeight) {
  int N = sprites.size();

  int i=0;
  for(auto sprite : sprites) {
    char c = text[i];
    float aspect = characters.getDim(c).x/characters.getDim(c).y;

    glm::vec2 spritePos;
    if(align==TextAlign::ALIGN_CENTER) {
      spritePos = {
        pos.x+(i-N/2)*charDims.x*2,
        pos.y+charDims.y/2
      };
    }
    else if(align==TextAlign::ALIGN_LEFT) {
      spritePos = {
        pos.x+charDims.x/2 + i*charDims.x*2,
        pos.y+charDims.y/2
      };
    }
    else if(align==TextAlign::ALIGN_RIGHT) {
      spritePos = {
        pos.x-charDims.x*N+charDims.x/2 + i*charDims.x*2,
        pos.y+charDims.y/2
      };
    }

    sprite->setColor(color);
    sprite->render(spritePos, charDims*glm::vec2(aspect,1));
    i++;
  }
}

void TextLine::destroy() {
  for(auto sprite : sprites) {
    sprite->setTexture(0);
    sprite->destroy();
    delete sprite;
  }
}

void TextLine::destroyAll() {
  textSpriteProgram->destroy();
}
