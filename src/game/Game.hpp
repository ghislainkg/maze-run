/*
Game.hpp
-----------------------------------------------------

Copyright (c) 2023, Ghislain Kengne Gumete
All rights reserved.

License follow the 4-clause BSD license.

Created on: 29 august 2023
    Author: Ghislain Kengne Gumete
      Mail: kengneghislain98@gmail.com

*/

#ifndef _MAZE_RUN_GAME_
#define _MAZE_RUN_GAME_

#include "handlers/GameLoop.hpp"
#include "handlers/GameEditor.hpp"
#include "rendering/GameRenderer.hpp"

#endif
