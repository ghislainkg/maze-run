/*
Main.cpp
-----------------------------------------------------

Copyright (c) 2023, Ghislain Kengne Gumete
All rights reserved.

License follow the 4-clause BSD license.

Created on: 29 august 2023
    Author: Ghislain Kengne Gumete
      Mail: kengneghislain98@gmail.com

*/

#include "window/GameWindow.hpp"

int main() {
  std::cout << "Start" << std::endl;
  GameWindow window;
  window.initGLFW();
  window.loop();
  window.clear();
  window.end();
}
