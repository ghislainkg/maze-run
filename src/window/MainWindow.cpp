/*
MainWindow.cpp
-----------------------------------------------------

Copyright (c) 2023, Ghislain Kengne Gumete
All rights reserved.

License follow the 4-clause BSD license.

Created on: 29 august 2023
    Author: Ghislain Kengne Gumete
      Mail: kengneghislain98@gmail.com

*/

#include "MainWindow.hpp"

void MainWindow::initGLFW() {
  std::cout << "init GLFW" << std::endl;
  if(!glfwInit()) {
    std::cerr << "ERROR: Failed to init GLFW" << std::endl;
    std::exit(EXIT_FAILURE);
  }

  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);

  window = glfwCreateWindow(
    width, height,
    "Jungle Run",
    nullptr, nullptr);
  if(!window) {
    std::cerr << "ERROR: Failed to open window" << std::endl;
    glfwTerminate();
    std::exit(EXIT_FAILURE);
  }

  glfwMakeContextCurrent(window);

  if(!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
    std::cerr << "ERROR: Failed to initialize OpenGL context" << std::endl;
    glfwTerminate();
    std::exit(EXIT_FAILURE);
  }

  afterInit();
}

void MainWindow::loop() {
  while(keep && !glfwWindowShouldClose(window)) {
    glfwSwapBuffers(window);

    float currentTime = glfwGetTime();
    float deltaTime = currentTime-lastTime;
    lastTime = currentTime;


    keep = frame(deltaTime);

    glfwPollEvents();
    processKeys();
  }
}

void MainWindow::processKeys() {
  int state;
  // KEYBOARD
  state = glfwGetKey(window, GLFW_KEY_UP);
  if(state == GLFW_PRESS) {
    onControlKey(ControlKey::UP);
  }
  state = glfwGetKey(window, GLFW_KEY_DOWN);
  if(state == GLFW_PRESS) {
    onControlKey(ControlKey::DOWN);
  }
  state = glfwGetKey(window, GLFW_KEY_LEFT);
  if(state == GLFW_PRESS) {
    onControlKey(ControlKey::LEFT);
  }
  state = glfwGetKey(window, GLFW_KEY_RIGHT);
  if(state == GLFW_PRESS) {
    onControlKey(ControlKey::RIGHT);
  }
  state = glfwGetKey(window, GLFW_KEY_SPACE);
  if(state == GLFW_PRESS) {
    onControlKey(ControlKey::SPACE);
  }
  state = glfwGetKey(window, GLFW_KEY_E);
  if(state == GLFW_PRESS) {
    onControlKey(ControlKey::E);
  }
  state = glfwGetKey(window, GLFW_KEY_P);
  if(state == GLFW_PRESS) {
    onControlKey(ControlKey::P);
  }
  state = glfwGetKey(window, GLFW_KEY_S);
  if(state == GLFW_PRESS) {
    onControlKey(ControlKey::S);
  }
  state = glfwGetKey(window, GLFW_KEY_L);
  if(state == GLFW_PRESS) {
    onControlKey(ControlKey::L);
  }
  state = glfwGetKey(window, GLFW_KEY_M);
  if(state == GLFW_PRESS) {
    onControlKey(ControlKey::M);
  }

  if(glfwGetWindowAttrib(window, GLFW_HOVERED)) {
    // If the mouse in inside the window

    // MOUSE BUTTONS
    state = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT);
    if(state == GLFW_PRESS) {
      onControlKey(ControlKey::MOUSE_LEFT);
    }
    state = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT);
    if(state == GLFW_PRESS) {
      onControlKey(ControlKey::MOUSE_RIGHT);
    }
  
    // MOUSE MOVEMENTS
    double x, y;
    glfwGetCursorPos(window, &x, &y);
    if(mousePosX!=x || mousePosY!=y) {
      mousePosX = x;
      mousePosY = y;
      onMouseMove(mousePosX, mousePosY);
    }
  }
}

void MainWindow::end() {
  glfwDestroyWindow(window);
  glfwTerminate();
}

void MainWindow::quit() {
  keep = false;
}
