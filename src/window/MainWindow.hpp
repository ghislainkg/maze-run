/*
MainWindow.hpp
-----------------------------------------------------

Copyright (c) 2023, Ghislain Kengne Gumete
All rights reserved.

License follow the 4-clause BSD license.

Created on: 29 august 2023
    Author: Ghislain Kengne Gumete
      Mail: kengneghislain98@gmail.com

*/

#ifndef _MAZE_RUN__WINDOW_MAIN_WINDOW_
#define _MAZE_RUN__WINDOW_MAIN_WINDOW_

#include "../game/Game.hpp"

enum ControlKey {
  UP, DOWN, LEFT, RIGHT, SPACE,
  MOUSE_RIGHT, MOUSE_LEFT,
  E, P, S, L, M
};

inline std::string getControlKeyName(ControlKey key) {
  switch(key) {
    case UP:
      return "UP";
    case DOWN:
      return "DOWN";
    case LEFT:
      return "LEFT";
    case RIGHT:
      return "RIGHT";
    case SPACE:
      return "SPACE";
    case MOUSE_LEFT:
      return "MOUSE_LEFT";
    case MOUSE_RIGHT:
      return "MOUSE_RIGHT";
    case E:
      return "E";
    case P:
      return "P";
    case S:
      return "S";
    case L:
      return "L";
  }
  return "unknown";
}

class MainWindow {
public:
  MainWindow() {}

  void initGLFW();
  void loop();
  void end();

protected:
  int width = 800;
  int height = 600;

  virtual bool frame(float deltaTime) = 0;
  virtual void afterInit() = 0;

  virtual void onControlKey(ControlKey key) {};
  virtual void onMouseMove(float x, float y) {};

  void quit();

private:
  GLFWwindow * window;
  float lastTime = 0;
  float mousePosX, mousePosY;

  bool keep = true;

  void processKeys();
};

#endif
