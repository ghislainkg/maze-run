/*
GameWindow.hpp
-----------------------------------------------------

Copyright (c) 2023, Ghislain Kengne Gumete
All rights reserved.

License follow the 4-clause BSD license.

Created on: 29 august 2023
    Author: Ghislain Kengne Gumete
      Mail: kengneghislain98@gmail.com

*/

#ifndef _MAZE_RUN__WINDOW_GAME_WINDOW_
#define _MAZE_RUN__WINDOW_GAME_WINDOW_

#include "MainWindow.hpp"

enum GameWindowMode {
  EDIT, PLAY, WIN_SCREEN, LOOSE_SCREEN, MENU_SCREEN
};

struct Button {
  TextLinePtr text = nullptr;
  glm::vec2 pos = {0,0};
  glm::vec2 dims = {0,0};
  glm::vec3 color = {0,0,0};
  glm::vec3 colorHover = {0,0,0};

  bool isHovering(const glm::vec2 & click) {
    return glm::abs(click-pos).x <= dims.x*text->getText().size() &&
        glm::abs(click-pos).y <= dims.y;
  }

  void init(const TextCharacters & arialCharacters) {
    text = std::make_shared<TextLine>(arialCharacters);
    text->setTextAlign(TextAlign::ALIGN_CENTER);
    text->setColor(color);
    text->setPosition(pos);
    text->setCharDims(dims);
  }

  void hover(const glm::vec2 & mouse) {
    if(text) {
      if(isHovering(mouse)) {
        text->setColor(colorHover);
      }
      else {
        text->setColor(color);
      }
    }
  }

  void destroy() {
    if(text) text->destroy();
  }
};

class GameWindow : public MainWindow {
public:
  GameWindow() {}

  void clear();

protected:

  bool frame(float deltaTime) override;
  void afterInit() override;

  void onControlKey(ControlKey key) override;
  void onMouseMove(float x, float y) override;

private:
  GameRenderer renderer;
  MapPtr map;
  GameLoop gameLoop;
  GameEditor gameEditor;

  TextCharacters arialCharacters;

  GameWindowMode mode = PLAY;

  float beforeGameLoop = 0;
  float playDelay = 0.3; // seconds

  glm::vec2 mouseScreenPos = {0,0};
  glm::vec2 mouseWorldPos = {0,0};
  glm::uvec2 mouseGridPos = {0,0};

  TextLinePtr textLifes = nullptr;
  TextLinePtr textPoints = nullptr;

  Button menuButton = {
    nullptr,
    {0.0,-0.1},
    {0.04,0.04},
    COLOR_GREEN,
    COLOR_GRAY
  };

  void startPlay();
  void updatePlayTexts();

  TextLinePtr textWin = nullptr;
  void startWin();

  TextLinePtr textLoose = nullptr;
  void startLoose();

  TextLinePtr textEditInstructions0;
  TextLinePtr textEditInstructions1;
  TextLinePtr textEditInstructions2;
  TextLinePtr textEditInstructions3;
  TextLinePtr textEditInstructions4;
  TextLinePtr textEditInstructions5;
  void startEdit();

  Button menuPlayButton = {
    nullptr,
    {0.0, 0.3},
    {0.04,0.04},
    COLOR_ORANGE,
    COLOR_GRAY
  };
  Button menuEditButton = {
    nullptr,
    {0.0, 0.1},
    {0.04,0.04},
    COLOR_ORANGE,
    COLOR_GRAY
  };
  Button menuQuitButton = {
    nullptr,
    {0.0,-0.2},
    {0.04,0.04},
    COLOR_ORANGE,
    COLOR_GRAY
  };
  void startMenu();
};

#endif
