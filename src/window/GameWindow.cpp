/*
GameWindow.cpp
-----------------------------------------------------

Copyright (c) 2023, Ghislain Kengne Gumete
All rights reserved.

License follow the 4-clause BSD license.

Created on: 29 august 2023
    Author: Ghislain Kengne Gumete
      Mail: kengneghislain98@gmail.com

*/

#include "GameWindow.hpp"

bool GameWindow::frame(float deltaTime) {
  if(mode==GameWindowMode::PLAY) {
    updatePlayTexts();
    renderer.renderMap(map, width, height);
  }
  else if(mode==GameWindowMode::WIN_SCREEN || mode==GameWindowMode::LOOSE_SCREEN) {
    renderer.renderMap(nullptr, width, height);
  }
  else if(mode==GameWindowMode::EDIT) {
    renderer.renderMap(map, width, height);
  }
  else if(mode==GameWindowMode::MENU_SCREEN) {
    renderer.renderMap(nullptr, width, height);
  }

  beforeGameLoop -= deltaTime;
  if(beforeGameLoop<=0) {
    gameLoop.loop();
    beforeGameLoop = playDelay;

    if(gameLoop.getGameState()==GameState::WIN && mode==GameWindowMode::PLAY) {
      mode = GameWindowMode::WIN_SCREEN;
      startWin();
    }
    else if(gameLoop.getGameState()==GameState::LOOSE && mode==GameWindowMode::PLAY) {
      mode = GameWindowMode::LOOSE_SCREEN;
      startLoose();
    }
  }

  return true;
}

void GameWindow::afterInit() {
  renderer.initSprites();
  arialCharacters.init("assets/fonts/arial_bold.ttf", 64, 64, 128);
  mode = GameWindowMode::MENU_SCREEN;
  startMenu();
}

void GameWindow::startPlay() {
  std::string message;
  map = Map::load("map.game", message);
  gameEditor.setMap(nullptr);
  gameLoop.setMap(map);
  Player * player = static_cast<Player*>(gameLoop.getPlayer().get());
  player->setLife(4);
  player->setPoints(0);
  map->print();

  beforeGameLoop = playDelay;

  textLifes = std::make_shared<TextLine>(arialCharacters);
  textLifes->setTextAlign(TextAlign::ALIGN_LEFT);
  textLifes->setColor(COLOR_ORANGE);
  textLifes->setPosition({-0.9,0.9});
  textLifes->setCharDims({0.02f, 0.02f});

  textPoints = std::make_shared<TextLine>(arialCharacters);
  textPoints->setTextAlign(TextAlign::ALIGN_LEFT);
  textPoints->setColor(COLOR_YELLOW);
  textPoints->setPosition({-0.9,0.85});
  textPoints->setCharDims({0.02f, 0.02f});

  renderer.textLines.clear();
  renderer.textLines.push_back(textLifes);
  renderer.textLines.push_back(textPoints);
}
void GameWindow::updatePlayTexts() {
  Player * player = static_cast<Player*>(gameLoop.getPlayer().get());

  textLifes->updateText(std::string("LIVES : ") + std::to_string(player->getLifeCount()));
  textPoints->updateText(std::string("POINTS : ") + std::to_string(player->getPoints()));
}

void GameWindow::startEdit() {
  std::string message;
  map = Map::load("map.game", message);
  gameEditor.setMap(map);
  gameLoop.setMap(nullptr);
  map->print();

  glm::vec2 dims = {0.01f, 0.02f};

  textEditInstructions0 = std::make_shared<TextLine>(arialCharacters);
  textEditInstructions0->setTextAlign(TextAlign::ALIGN_LEFT);
  textEditInstructions0->setColor(COLOR_BLUE);
  textEditInstructions0->setPosition({-0.9,0.9});
  textEditInstructions0->setCharDims(dims);

  textEditInstructions1 = std::make_shared<TextLine>(arialCharacters);
  textEditInstructions1->setTextAlign(TextAlign::ALIGN_LEFT);
  textEditInstructions1->setColor(COLOR_BLUE);
  textEditInstructions1->setPosition({-0.9,0.85});
  textEditInstructions1->setCharDims(dims);

  textEditInstructions2 = std::make_shared<TextLine>(arialCharacters);
  textEditInstructions2->setTextAlign(TextAlign::ALIGN_LEFT);
  textEditInstructions2->setColor(COLOR_BLUE);
  textEditInstructions2->setPosition({-0.9,0.80});
  textEditInstructions2->setCharDims(dims);

  textEditInstructions3 = std::make_shared<TextLine>(arialCharacters);
  textEditInstructions3->setTextAlign(TextAlign::ALIGN_LEFT);
  textEditInstructions3->setColor(COLOR_BLUE);
  textEditInstructions3->setPosition({-0.9,0.75});
  textEditInstructions3->setCharDims(dims);

  textEditInstructions4 = std::make_shared<TextLine>(arialCharacters);
  textEditInstructions4->setTextAlign(TextAlign::ALIGN_LEFT);
  textEditInstructions4->setColor(COLOR_BLUE);
  textEditInstructions4->setPosition({-0.9,0.70});
  textEditInstructions4->setCharDims(dims);

  textEditInstructions5 = std::make_shared<TextLine>(arialCharacters);
  textEditInstructions5->setTextAlign(TextAlign::ALIGN_LEFT);
  textEditInstructions5->setColor(COLOR_BLUE);
  textEditInstructions5->setPosition({-0.9,0.65});
  textEditInstructions5->setCharDims(dims);

  renderer.textLines.clear();
  renderer.textLines.push_back(textEditInstructions0);
  renderer.textLines.push_back(textEditInstructions1);
  renderer.textLines.push_back(textEditInstructions2);
  renderer.textLines.push_back(textEditInstructions3);
  renderer.textLines.push_back(textEditInstructions4);
  renderer.textLines.push_back(textEditInstructions5);

  textEditInstructions0->updateText("MOUSE LEFT BUTTON TO ADD STUFF");
  textEditInstructions1->updateText("MOUSE RIGHT BUTTON TO ERASE");
  textEditInstructions2->updateText("P TO PLAY");
  textEditInstructions3->updateText("M TO GO BACK TO MENU");
  textEditInstructions4->updateText("S TO SAVE");
  textEditInstructions5->updateText("L TO RELOAD");
}

void GameWindow::startWin() {
  menuButton.init(arialCharacters);

  textWin = std::make_shared<TextLine>(arialCharacters);
  textWin->setTextAlign(TextAlign::ALIGN_CENTER);
  textWin->setColor(COLOR_BLUE);
  textWin->setPosition({0.0,0.0});
  textWin->setCharDims({0.05f, 0.05f});

  renderer.textLines.clear();
  renderer.textLines.push_back(menuButton.text);
  renderer.textLines.push_back(textWin);

  textWin->updateText("! YOU WIN !");
  menuButton.text->updateText("MENU");
}

void GameWindow::startLoose() {
  menuButton.init(arialCharacters);

  textLoose = std::make_shared<TextLine>(arialCharacters);
  textLoose->setTextAlign(TextAlign::ALIGN_CENTER);
  textLoose->setColor(COLOR_RED);
  textLoose->setPosition({0.0,0.0});
  textLoose->setCharDims({0.05f, 0.05f});

  renderer.textLines.clear();
  renderer.textLines.push_back(menuButton.text);
  renderer.textLines.push_back(textLoose);

  textLoose->updateText("YOU LOOSE :[");
  menuButton.text->updateText("MENU");
}

void GameWindow::startMenu() {
  menuPlayButton.init(arialCharacters);
  menuEditButton.init(arialCharacters);
  menuQuitButton.init(arialCharacters);

  renderer.textLines.clear();
  renderer.textLines.push_back(menuPlayButton.text);
  renderer.textLines.push_back(menuEditButton.text);
  renderer.textLines.push_back(menuQuitButton.text);

  menuPlayButton.text->updateText("PLAY");
  menuEditButton.text->updateText("EDIT");
  menuQuitButton.text->updateText("QUIT");
}

void GameWindow::onControlKey(ControlKey key) {
  std::cout << "Press key " << getControlKeyName(key) << std::endl;
  if(mode == PLAY) {
    if(key==UP || key==DOWN || key==LEFT || key==RIGHT) {
      glm::vec2 move = {0,0};
      if(key==ControlKey::UP) move = {0, -1};
      if(key==ControlKey::DOWN) move = {0, 1};
      if(key==ControlKey::LEFT) move = {-1, 0};
      if(key==ControlKey::RIGHT) move = {1, 0};
      gameLoop.onPlayerMove(move);
    }
    else if(key == E) {
      mode = GameWindowMode::EDIT;
      startEdit();
    }
  }
  else if(mode == EDIT) {
    if(key == MOUSE_LEFT) {
      gameEditor.onClickAddEntity(mouseGridPos);
    }
    else if(key == MOUSE_RIGHT) {
      gameEditor.onClickEraseEntity(mouseGridPos);
    }
    else if(key == P) {
      mode = GameWindowMode::PLAY;
      startPlay();
    }
    else if(key == M) {
      mode = GameWindowMode::MENU_SCREEN;
      startMenu();
    }
    else if(key == S) {
      std::string message;
      map->save("map.game", message);
    }
    else if(key == L) {
      startEdit();
    }
  }
  else if(mode == GameWindowMode::MENU_SCREEN) {
    if(key == MOUSE_LEFT) {
      if(menuPlayButton.isHovering(mouseWorldPos)) {
        mode = GameWindowMode::PLAY;
        startPlay();
      }
      else if(menuEditButton.isHovering(mouseWorldPos)) {
        mode = GameWindowMode::EDIT;
        startEdit();
      }
      else if(menuQuitButton.isHovering(mouseWorldPos)) {
        quit();
      }
    }
  }
  else if(mode == GameWindowMode::WIN_SCREEN) {
    if(key == MOUSE_LEFT) {
      if(menuButton.isHovering(mouseWorldPos)) {
        mode = GameWindowMode::MENU_SCREEN;
        startMenu();
      }
    }
  }
  else if(mode == GameWindowMode::LOOSE_SCREEN) {
    if(key == MOUSE_LEFT) {
      if(menuButton.isHovering(mouseWorldPos)) {
        mode = GameWindowMode::MENU_SCREEN;
        startMenu();
      }
    }
  }
}

void GameWindow::onMouseMove(float x, float y) {
  mouseScreenPos = {x,y};
  mouseWorldPos = {
    (x/width - 0.5)*2.0,
    (-y/height + 0.5)*2.0
  };
  if(map) {
    mouseGridPos = renderer.fromMouseScreenPosToGridPos(map, mouseScreenPos, width, height);
  }

  menuButton.hover(mouseWorldPos);
  menuPlayButton.hover(mouseWorldPos);
  menuEditButton.hover(mouseWorldPos);
  menuQuitButton.hover(mouseWorldPos);
}

void GameWindow::clear() {
  arialCharacters.destroy();

  if(textLifes) textLifes->destroy();
  if(textLoose) textLoose->destroy();
  if(textPoints) textPoints->destroy();
  if(textWin) textWin->destroy();

  menuButton.destroy();
  menuPlayButton.destroy();
  menuEditButton.destroy();
  menuQuitButton.destroy();

  Sprite::destroyAll();
  TextLine::destroyAll();
}
